package kz.ginayah.basmeshit

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BasmeshitApplication

fun main(args: Array<String>) {
    runApplication<BasmeshitApplication>(*args)
}
