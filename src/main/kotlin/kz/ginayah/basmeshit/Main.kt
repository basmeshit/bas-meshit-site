package kz.ginayah.basmeshit

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class Main {
    @GetMapping
    fun index(): String {
        return "index"
    }
}